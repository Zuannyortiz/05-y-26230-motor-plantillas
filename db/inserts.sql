-- Inserts para la tabla Integrantes
INSERT INTO integrantes (nombre, apellido, matricula, pagina, activo, orden) VALUES
    ('Zuanny Librada', 'Ortiz Quintana', 'Y26230', '/Y26230', 1, 1),
    ('Gabriela', 'Espinola Alarcón', 'Y23715', '/Y23715', 1, 2),
    ('Nicolas Sebastian', 'Gimenez Campos', 'Y18433', '/Y18433', 1, 3),
    ('Yenifer Araceli', 'Aguilera Duarte', 'Y12954', '/Y12954', 1, 4),
    ('Diego Anastacio', 'Ramirez', 'UG0045', '/UG0045', 1, 5);

INSERT INTO tipoMedia (nombre, activo, orden) VALUES
    ('YouTube', 1, 1),
    ('Imagen', 1, 2),
    ('Dibujo', 1, 3);

INSERT INTO media (nombre, src, url, titulo, tipoMedia, matricula, activo, orden) VALUES
     ('YouTube', NULL, 'https://www.youtube.com/embed/Qes1RMK9a50?si=U5YTt8otRVQc1Ahk', 'Video favorito de YouTube', 1, 'Y26230', 1, 1),
     ('Imagen', '/Images/Extrovertida.jpg', NULL, 'Imagen que me representa', 2, 'Y26230', 1, 2),
     ('Dibujo', '/Images/Dibujo_Zuanny.png', NULL,'Mi dibujo', 3, 'Y26230', 1, 3),

    ('YouTube', NULL, 'https://www.youtube.com/embed/Rk7gnFCeVAY', NULL, 1, 'Y23715', 1, 4),
    ('Imagen', '/Images/Bob%20Esponja.jpeg',NULL, NULL, 2, 'Y23715', 1, 5),
    ('Dibujo', '/Images/bananamichi.png', NULL, NULL, 3, 'Y23715', 1, 6),

    ('YouTube', NULL, 'https://www.youtube.com/embed/YZ8j6iO0ulw?si=3qmb7GQ0CxHN2C3Q', NULL, 1, 'Y18433', 1, 7),
    ('Imagen', '/Images/mis_sueños.jpeg', NULL, NULL, 2, 'Y18433', 1, 8),
    ('Dibujo', '/Images/DibujoNico.png', NULL, NULL, 3, 'Y18433', 1, 9),

    ('YouTube', NULL, 'https://www.youtube.com/embed/DjeGUE9ya5s?si=Y26o_gWUj7Ktbw94', NULL, 1, 'Y12954', 1, 10),
    ('Imagen', NULL, 'Imagen que Yenifer no subio', NULL, 2, 'Y12954', 1, 11),
    ('Dibujo', NULL, 'Imagen que Yenifer no subio', NULL, 3, 'Y12954', 1, 12),

     ('YouTube', NULL, 'https://www.youtube.com/embed/210R0ozmLwg?si=bbjXLbzQS8fBaoTz', NULL, 1, 'UG0045', 1,13),
     ('Imagen', '/Images/imagen1Diego.jpeg', NULL, NULL, 2, 'UG0045', 1, 14),
     ('Dibujo', '/Images/Imagen2Diego.jpeg', NULL, NULL, 3, 'UG0045', 1, 15);

INSERT INTO curso (nombre, url, src, activo) VALUES
    ('imagen', NULL, '/Images/uclogo.png', 1);
