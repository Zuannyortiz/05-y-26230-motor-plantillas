CREATE TABLE IF NOT EXISTS "integrantes" (
    "id" INTEGER NOT NULL UNIQUE,
    "orden" INT,
    "nombre" TEXT,
    "apellido" TEXT,
    "matricula" TEXT,
    "pagina" TEXT,
    "activo" BOOLEAN DEFAULT FALSE,
    PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS "tipoMedia" (
    "id" INTEGER NOT NULL UNIQUE,
    "orden" INT,
    "nombre" TEXT,
    "activo" BOOLEAN DEFAULT FALSE,
    PRIMARY KEY("id")
);

CREATE TABLE IF NOT EXISTS "media" (
     "id" INTEGER NOT NULL UNIQUE,
     "orden" INT,
     "nombre" TEXT,
     "src" TEXT,
     "url" TEXT,
     "titulo" TEXT,
     "tipoMedia" INTEGER,
     "matricula" TEXT,
     "activo" BOOLEAN DEFAULT FALSE,
     PRIMARY KEY("id"),
        FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
        ON UPDATE NO ACTION ON DELETE NO ACTION,
        FOREIGN KEY ("tipoMedia") REFERENCES "tipoMedia"("id")
        ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS "curso" (
    "id" INTEGER NOT NULL UNIQUE,
    "nombre" TEXT,
    "url" TEXT,
    "src" TEXT,
    "activo" BOOLEAN DEFAULT FALSE,
    PRIMARY KEY("id")
);