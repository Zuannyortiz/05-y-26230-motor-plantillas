const express = require ("express");
const router = express.Router();
const {getAll} = require("../db/conexion");
require("dotenv").config();

router.use((req, res, next) => {
    res.locals.repositorio = process.env.ENLACEGIT;
    res.locals.nombre = process.env.NOMBRE;
    res.locals.materia = process.env.DATOSMATERIA;
    next();
});
router.get("/", async (request, response) => {
    const rows = await getAll("SELECT * FROM integrantes WHERE activo = 1");
    response.render("index", {
        integrantes: rows,
    });
});



router.get("/curso", async (request, response)=>{
    const rows = await getAll("select * from integrantes")
    response.render("curso",{
        integrantes: rows,
    })
});

router.get("/word_cloud", async (request, response)=>{
    const rows = await getAll("select * from integrantes")
    response.render("word_cloud",{
        integrantes: rows,
    })
});

router.get("/logo", (request, response)=>{
    response.render("code_bravo")
});

/*const matriculas = [...new Set(db.media.map(item => item.matricula))];*/

/* router.get("/:matricula", async (request, response) => {
    const matricula = request.params.matricula;
    if (matriculas.includes(matricula)) {
        const mediaFiltrada = db.media.filter(item => item.matricula === matricula);
        const integrantesFiltrados = db.integrantes.filter(item => item.matricula === matricula);
        const rows = await getAll("select * from integrantes")
        response.render('integrantes', {
            tipoMedia: db.tipoMedia,
            integrante: integrantesFiltrados,
            media: mediaFiltrada,
            integrantes: rows,
        });
    } else {
        // Si la matrícula no se encuentra, responder con un error 404
        response.status(404).render('mensaje');
    }
});*/
router.get("/:matricula", async (request, response, next) => {
    const matriculas = (await getAll("select matricula from integrantes where activo = 1 order by orden")).map(obj => obj.matricula);
    const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by orden");
    const matricula = request.params.matricula;
    const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");

    if (matriculas.includes(matricula)) {
        const integranteFilter = await getAll("select * from integrantes where activo = 1 and matricula = ? order by orden", [matricula]);
        const mediaFilter = await getAll("select * from media where activo = 1 and matricula = ? order by orden", [matricula]);
        response.render('integrantes', {
            integrante: integranteFilter,
            tipoMedia: tipoMedia,
            media: mediaFilter,
            integrantes: integrantes,
        });
    } else {
        next();
    }
});
module.exports = router;
