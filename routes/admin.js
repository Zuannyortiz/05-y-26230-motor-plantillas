const express = require("express");
const router = express.Router();
const multer = require("multer");
const upload = multer({ dest: "./public/Images" });
const fileUpload = upload.single('imagen');
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

const IntegratesController = require('../controllers/admin/integrantes.controller');
const TipMediaController = require('../controllers/admin/tipoMedia.controller');
const MediaController = require('../controllers/admin/media.controller');

router.get("/", (req, res) => {
    res.render("admin/index");
});

// Integrantes
router.get('/integrantes/listar', IntegratesController.index);
router.get('/integrantes/crear', IntegratesController.create);
router.post('/integrantes/create', IntegratesController.store);
router.post('/integrantes/delete/:matricula', IntegratesController.destroy);
router.get('/integrantes/edit/:id', IntegratesController.edit);
router.post('/integrantes/update/:id', IntegratesController.update);


// Tipo media
router.get('/tipoMedia/listar', TipMediaController.index);
router.get('/tipoMedia/crear', TipMediaController.create);
router.post("/tipoMedia/create", TipMediaController.store);
router.post('/tipoMedia/delete/:id', TipMediaController.destroy);
router.get('/tipoMedia/edit/:id', TipMediaController.edit);
router.post('/tipoMedia/update/:id', TipMediaController.update);

// Media
router.get('/media/listar', MediaController.index);
router.get("/media/crear", MediaController.create);
router.post("/media/create", fileUpload,MediaController.store);
router.post('/media/delete/:id', MediaController.destroy);
router.get('/media/edit/:id', MediaController.edit);
router.post('/media/update/:id',fileUpload,MediaController.update);
module.exports = router;
