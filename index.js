const express = require("express");
const bodyParser = require("body-parser");
const hbs = require("hbs");
require("dotenv").config();
const router = require('./routes/public');
const routerAdmin = require('./routes/admin');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use("/", router);
app.use("/admin", routerAdmin);
app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs');
app.set("views", __dirname + "/views");
hbs.registerPartials(__dirname + "/views/partials");

// Registrar el helper 'eq'
hbs.registerHelper('eq', function(a, b) {
    return a === b;
});

hbs.registerHelper('containsId', function(media, id) {
    return media.some(function(mediaItem) {
        return mediaItem.id === id;
    });
});


const puerto = process.env.PORT || 3000;
app.listen(puerto, () => {
    console.log(`Servidor corriendo en http://localhost:${puerto}`);
});
