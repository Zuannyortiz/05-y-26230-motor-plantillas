/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/

const {getAll, getLastOrder, run, getLastId, asociacionTipoMedia} = require("../../db/conexion");
const TipoMediaControllers = {

    //Listado de integrantes de /admin/tipoMedia

    index: async function (req, res) {
        try {
            // Listar solo los integrantes activos
            const tipoMedia = await getAll('SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY orden');
            res.render('admin/tipoMedia/index', {
                tipoMedia: tipoMedia
            });
        } catch (error) {
            console.error('Error al obtener los tipo media:', error);
            res.status(500).send('Error al obtener tipo media');
        }

    },

    create: async function (req, res) {
        res.render("admin/tipoMedia/crearTipoMedia");
    },


    store: async function (req,res) {
        const ultimoOrden = await getLastOrder("tipoMedia");
        const nuevoOrden = ultimoOrden + 1;
        const ultimoId = await getLastId("tipoMedia");
        const nuevoId = ultimoId + 1;

        const { nombre, activo } = req.body;
        const isActive = activo ? 1 : 0;

        if (!nombre) {
            return res.status(400).json({ error: 'Debes completar todos los campos.' });
        }

        try {
            await run(`INSERT INTO tipoMedia (orden, id, nombre, activo) VALUES (?, ?, ?, ?)`,
                [nuevoOrden, nuevoId, nombre, isActive]);
            res.json({ success: true });
        } catch (error) {
            console.error('Error al crear tipo media:', error);
            res.status(500).json({ error: 'Error al crear tipo media' });
        }
    },


    update: async function (req, res) {
        const id = parseInt(req.params.id);
        if (!req.body.nombre) {
            return res.status(400).json({error: 'Debes completar todos los campos.'});
        }
        try {
            await run("UPDATE tipoMedia SET nombre = ? WHERE id = ?",
                [
                    req.body.nombre,
                    id
                ]);
            res.json({success: true, message: 'Registro actualizado correctamente.'});
        } catch (error) {
            console.error('Error al actualizar el integrante:', error);
            res.status(500).json({error: '¡Error al actualizar al integrante!'});
        }
    },


    edit: async function (req, res) {
        try {
            const id = parseInt(req.params.id);
            const tipoMedia = await getAll('SELECT * FROM tipoMedia WHERE id = ?', [id]);
            if (!tipoMedia) {
                return res.status(404).send('Tipo media no encontrado');
            }
            res.render('admin/tipoMedia/tipoMediaEditform', {
                tipoMedia: tipoMedia
            });
        } catch (error) {
            console.error('Error al obtener el tipo media:', error);
            res.status(500).send('Error al obtener el tipo media');
        }
    },

    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        console.log("tipoMedia", id);
        if (await asociacionTipoMedia(id)) {
            res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡No se puede eliminar el resgistro, esta información ya esta siendo utilizada en otras entidades!'));
        } else {
            try {
                await run("UPDATE tipoMedia SET activo = 0 WHERE id = ?", [id]);
                res.redirect("/admin/tipoMedia/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (error) {
                console.error('Error al eliminar el Tipo Media:', error);
                res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }

    },

};

module.exports = TipoMediaControllers;