/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/

const {getAll, getLastOrder, run, asociacionTipoMedia, getOne} = require("../../db/conexion");
const fs = require("fs").promises;
const MediaControllers = {

    //Listado de integrantes de /admin/tipoMedia

    index: async function (req, res) {
        try {
            const media = await getAll(`
            SELECT media.*, tipoMedia.nombre as tipoMediaNombre, integrantes.nombre as integranteNombre, integrantes.apellido as integranteApellido
            FROM media
            LEFT JOIN tipoMedia ON media.tipoMedia = tipoMedia.id
            LEFT JOIN integrantes ON media.matricula = integrantes.matricula
            WHERE media.activo = 1
            ORDER BY media.orden
        `);
            res.render("admin/media/index", {
                media: media
            });
        } catch (error) {
            console.error('Error al obtener los datos de media:', error);
            res.status(500).send('Error al obtener los datos de media');
        }

    },

    create: async function (req, res) {
        try {
            const integrantes = await getAll("SELECT * FROM integrantes WHERE activo = 1 ORDER BY nombre");
            const tipoMedia = await getAll("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY nombre");
            res.render("admin/media/crearMedia", {
                integrantes: integrantes,
                tipoMedia: tipoMedia,
            });
        } catch (error) {
            console.error('Error al obtener datos para crear media:', error);
            res.status(500).send('Error al obtener datos para crear media');
        }
    },


    store: async function (req,res) {
        // Extrae los datos del formulario
        const url = req.body.url;
        const titulo = req.body.titulo;
        const tipoMedia = req.body.tipoMedia;
        const integrante = req.body.integrante; // ID del integrante
        const activo = req.body.activo === 'on';
        let srcPath = "";
        const errors = [];

        // Validaciones
        if (!tipoMedia) errors.push("Debe ingresar el 'Tipo Media'.");
        if (!titulo) errors.push("Debe ingresar el 'Título'.");
        if (!integrante) errors.push("Debe ingresar el 'Integrante'.");

        // Verifica si se ha subido un archivo
        if (req.file) {
            const destino = `public/Images/${req.file.originalname}`;
            try {
                await fs.rename(req.file.path, destino);
                srcPath = `/Images/${req.file.originalname}`;
            } catch (err) {
                console.error(err);
                errors.push('¡Error al subir la imagen!');
            }
        }

        if (!url && !srcPath) {
            errors.push("Debe ingresar al menos uno de los siguientes campos: URL o SRC.");
        } else if (url && srcPath) {
            errors.push("Solo debe ingresar uno de los siguientes campos: URL o SRC.");
        }

        if (errors.length > 0) {
            return res.redirect(`/admin/media/crear?errors=${encodeURIComponent(JSON.stringify(errors))}`);
        }

        // Genera un nuevo orden basado en la última inserción
        const lastOrder = await getLastOrder('media');
        const newOrder = lastOrder + 1;

        try {
            const query = `
            insert into media (src, url, titulo, tipoMedia, matricula, activo, orden) select ?, ?, ?, ?, integrantes.matricula, ?, ? 
            from integrantes where integrantes.id = ?`;

            const params = [
                srcPath,
                url,
                titulo,
                tipoMedia,
                activo,
                newOrder,
                integrante
            ];
            await run(query, params);

            res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (err) {
            console.error(err);
            res.redirect(`/admin/media/crear?errors=${encodeURIComponent('¡Error al insertar el registro!')}`);
        }
    },


    update: async function (req, res) {
        const id = parseInt(req.params.id);
        if (!req.body.tipoMedia || !req.body.integrante) {
            return res.status(400).json({error: 'Debes completar todos los campos.'});
        }

        const mediaExistente = await getOne("SELECT * FROM media WHERE id = ?", [id]);
        try {
            // Verifica si se ha enviado un archivo
            let src = '';
            if (mediaExistente.src) {
                src = mediaExistente.src;
            }
            if (req.file) {
                var tpm_path = req.file.path;
                var destino = "public/Images/" + req.file.originalname;
                try {
                    await fs.rename(tpm_path, destino);
                    src = "/Images/" + req.file.originalname;
                } catch (err) {
                    return res.sendStatus(500);
                }
            }
            await run("UPDATE media SET src = ?, url = ?, titulo = ?, tipoMedia = ?, matricula = ? WHERE id = ?",
                [
                    src,
                    req.body.url,
                    req.body.titulo,
                    req.body.tipoMedia,
                    req.body.integrante,
                    id
                ]);
            res.json({success: true, message: 'Registro actualizado correctamente.'});
        } catch (error) {
            console.error('Error al actualizar el integrante:', error);
            res.status(500).json({error: '¡Error al actualizar al integrante!'});
        }
    },



    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        const integrantes = await getAll("SELECT * FROM integrantes WHERE activo = 1 ORDER BY nombre");
        const tipoMedia = await getAll("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY nombre");
        try {
            const media = await getAll("SELECT * FROM media WHERE id = ?", [id]);
            if (media.length === 0) {
                throw new Error('No se encontró el medio de comunicación');
            }

            const integranteSeleccionado = await getOne("SELECT * FROM integrantes WHERE matricula = ?", [media[0].matricula]);

            res.render("admin/media/mediaEditform", {
                integrantes: integrantes,
                tipoMedia: tipoMedia,
                media: media,
                integranteSeleccionado: integranteSeleccionado // Pasar el integrante seleccionado a la plantilla
            });
        } catch (err) {
            console.error(err);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al obtener el registro!'));
        }
    },



    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            await run("UPDATE media SET activo = 0 WHERE id = ?", [id]);
            res.redirect("/admin/media/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
        } catch (error) {
            console.error('Error al eliminar el Media:', error);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
        }
    }




};

module.exports = MediaControllers;