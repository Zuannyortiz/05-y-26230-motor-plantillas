/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/

const {getAll, getLastOrder, matriculaExistente, run, asociacion} = require("../../db/conexion");
const IntegrantesControllers = {

    //Listado de integrantes de /admin/integrantes
    index: async function (req, res) {
        try {
            const integrantes = await getAll('SELECT * FROM integrantes WHERE activo = 1 ORDER BY orden');
            res.render('admin/integrantes/index', {
                integrantes: integrantes
            });
        } catch (error) {
            console.error('Error al obtener los integrantes:', error);
            res.status(500).send('Error al obtener los integrantes');
        }

    },

    create: async function (req, res) {
        res.render("admin/integrantes/crearIntegrante");
    },


    store: async function (req,res) {
        const lastOrder = await getLastOrder("integrantes");
        const newOrder = lastOrder + 1;
        const {orden, nombre, apellido, matricula, pagina, activo} = req.body;
        const isActive = activo === 'on' ? 1 : 0;

        if (!nombre || !apellido || !matricula || !pagina) {
            return res.status(400).json({error: 'Debes completar todos los campos.'});
        }
        if (await matriculaExistente(req.body.matricula)) {
            return res.status(400).json({error: 'La matricula ya existe'});
        }

        try {
            await run(`INSERT INTO integrantes (orden, nombre, apellido, matricula, pagina, activo)
                       VALUES (?, ?, ?, ?, ?, ?)`,
                [
                    newOrder,
                    nombre,
                    apellido,
                    matricula,
                    pagina,
                    isActive
                ]);
            res.json({success: true});
        } catch (error) {
            console.error('Error al crear el integrante:', error);
            res.status(500).json({error: 'Error al crear el integrante'});
        }
    },

    update: async function (req, res) {
        const id = parseInt(req.params.id);
        if (!req.body.nombre || !req.body.apellido || !req.body.matricula || !req.body.pagina) {
            return res.status(400).json({error: 'Debes completar todos los campos.'});
        }
        try {
            await run("UPDATE integrantes SET nombre = ?, apellido = ?, matricula = ?, pagina = ? WHERE id = ?",
                [
                    req.body.nombre,
                    req.body.apellido,
                    req.body.matricula,
                    req.body.pagina,
                    id
                ]);
            res.json({success: true, message: 'Registro actualizado correctamente.'});
        } catch (error) {
            console.error('Error al actualizar el integrante:', error);
            res.status(500).json({error: '¡Error al actualizar al integrante!'});
        }
    },


    edit: async function (req, res) {
        try {
            const id = parseInt(req.params.id);
            const integrantes = await getAll('SELECT * FROM integrantes WHERE id = ?', [id]);
            if (!integrantes) {
                return res.status(404).send('Integrante no encontrado');
            }
            res.render('admin/integrantes/editform', {
                integrantes: integrantes
            });
        } catch (error) {
            console.error('Error al obtener el integrante:', error);
            res.status(500).send('Error al obtener el integrante');
        }
    },

    destroy: async function (req, res) {
        const matricula = req.params.matricula;
        console.log("matricula", matricula);

        if (await asociacion(matricula)) {
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡No se puede eliminar el resgistro, esta información ya esta siendo utilizada en otras entidades!'));
        } else {
            try {
                await run("UPDATE integrantes SET activo = 0 WHERE matricula = ?", [matricula]);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (error) {
                console.error('Error al eliminar el integrante:', error);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }
    },


};


module.exports = IntegrantesControllers;